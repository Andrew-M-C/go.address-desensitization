package main

import (
	"encoding/json"
	"time"

	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/regions"
	nlp "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/nlp/v20190408"
	logger "gitlab.com/Andrew-M-C/go.logger"
)

func main() {
	l := logger.NewStdout()
	defer cleanup(l)

	l.Infof("Hello, world!")

	credential := common.NewCredential(secretID, secretKey)
	cprof := profile.NewClientProfile()
	client, err := nlp.NewClient(credential, regions.Guangzhou, cprof)
	if err != nil {
		l.Errorf("nlp.NewClient error: %v", err)
		return
	}

	// 词法分析
	doLexicalAnalysis(l, client)

	l.Infof("Got client: %+v", client)
	return
}

// 词法分析
func doLexicalAnalysis(l logger.L, client *nlp.Client) (err error) {
	text := "湖州市吴兴区豪海华庭H组团23幢1单元1303"
	req := nlp.NewLexicalAnalysisRequest()
	req.Text = &text

	resp, err := client.LexicalAnalysis(req)
	if err != nil {
		l.Errorf("client.LexicalAnalysis error: %v", err)
		return
	}

	j, _ := json.Marshal(resp)
	l.Infof("Go response: %s", j)
	return
}

func cleanup(l logger.L) {
	l.Infof("Tool done.")
	time.Sleep(100 * time.Millisecond)
}
