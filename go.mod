module gitlab.com/Andrew-M-C/go.address-desensitization

go 1.15

require (
	gitlab.com/Andrew-M-C/go.logger v0.0.0-20201117120927-faffb61ece9b
	github.com/tencentcloud/tencentcloud-sdk-go v1.0.67
)
